		function validateSignUpForm() {
		 var name=document.getElementById("name").value;
		 var age=document.getElementById("age").value;
		 var gender=document.getElementById("gender").value;
		 var address=document.getElementById("address").value;
		 var mailid=document.getElementById("mailid").value;
		 var phonenumber=document.getElementById("phonenumber").value;
		 var invalidFormValue=new Array();
		 
		 if (isNull(name)) {
		 	invalidFormValue.push('Please Enter Name');
		 }
		 if (isNull(age)) {
		 	invalidFormValue.push('Please Enter Age');
		 }
		 if (isNull(address)) {
		 	invalidFormValue.push('Please Enter address');
		 }
		 	isGenderValid(gender,invalidFormValue);
		 	isMailValid(mailid,invalidFormValue);
		 	isPhoneNumberValid(phonenumber,invalidFormValue);
		 
		 var invalidvalues=invalidFormValue.toString().split(",").join("\n");
		 if(invalidFormValue.length>0){
		 swal("Error!", invalidvalues, "error");
		 	return false;
		 }
		 else {
		 return true;
		}
	}

	function validateLoginForm(){
		 var username=document.getElementById("user").value;
		 var password=document.getElementById("pass").value;
		 var invalidFormValue=new Array();
		 if (isNull(username)) {
		 	invalidFormValue.push('Please Enter username');
		 }
		 if (isNull(username)) {
		 	invalidFormValue.push('Please Enter Password');
		 }
		 var invalidvalues=invalidFormValue.toString().split(",").join("\n");
		 if(invalidFormValue.length>0){
		swal("Error!", invalidvalues, "error");
		 	return false;
		 }
		 else {
		 return true;
		}
	}

		function isNull(formValue)
		{
		 if((null === formValue)||(formValue.trim().length==0))
		 {
		 	return true;
		 }
		 return false;
		}
		function isGenderValid(formValue,invalidFormValue)
		{
			if (isNull(formValue)){
			invalidFormValue.push('Gender cannot be empty');
			}
			else if((formValue.toUpperCase() != 'MALE') && (formValue.toUpperCase() != "FEMALE") && (formValue.toUpperCase() != "OTHER")) {
		invalidFormValue.push('The Gender can be one of the following values male,female,other');
			}
		}
		function isMailValid(formValue,invalidFormValue)
		{
			var mailFormat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
			if (isNull(formValue)){
			invalidFormValue.push('Emailid cannot be empty');
			}
			else if(mailFormat.test(formValue)) {
		invalidFormValue.push('The given Mailid is not in pattern eg xyz@gmail.com');
			}
		}

		function isPhoneNumberValid(formValue,invalidFormValue)
		{
			var phomoformat = /^[0-9]{10}$/;
			if (isNull(formValue)){
			invalidFormValue.push('phonenumber cannot be empty');
			}
			else if(!phomoformat.test(formValue)) {
		invalidFormValue.push('phonenumber can have only digits values and range of 10');
			}
		}