package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.passengerInfo;
import com.example.repo.passengerRepository;

@RestController
@RequestMapping(path = "/user")
public class UserController {
	
	@Autowired
	passengerRepository passengerRepository;

	@GetMapping(path = "/get")
	public String getUser() {
		System.out.println("Inside get");
		System.out.println(passengerRepository.findAll().size());
		List<passengerInfo> list=passengerRepository.findAll();
		list.forEach(item->System.out.println(item.toString()));
		return "ok";
	}
}
