package com.example.controller;

import java.util.List;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.exception.TrainInfoException;
import com.example.exception.UserNotFoundException;
import com.example.model.TicketBooking;
import com.example.model.User;
import com.example.model.ticketReservationDB;
import com.example.model.trainInfo;
import com.example.service.RailwayService;

@Controller
public class LoginController {

	private static final Logger log = Logger.getLogger(LoginController.class);

	@Autowired
	RailwayService railwayservice;

	@GetMapping(value = { "/", "/index" })
	public String showIndexPage(Model model) {
		User user = new User();
		model.addAttribute("userdetails", user);
		return "index";
	}

	@PostMapping(path = { "/login" })
	public String doLogin(@RequestParam("user") String user, @RequestParam("pass") String pass, Model model,
			final RedirectAttributes redirectAttrs) throws UserNotFoundException {
		log.info("Getting Username " + user + " and Password " + pass);
		redirectAttrs.addFlashAttribute("username", user);
		redirectAttrs.addFlashAttribute("password", pass);
		railwayservice.isUSerValid(user, pass);
		return "redirect:/showtrain";
	}

	@PostMapping(path = { "/register", "/signup" })
	public String dosignup(@ModelAttribute("userdetails") User user, BindingResult result, Model model,
			final RedirectAttributes redirectAttrs) throws UserNotFoundException {

		log.info("User Details " + user.toString() + " to insert into DB");
		railwayservice.saveUserDetails(user);
		redirectAttrs.addFlashAttribute("username", user.getName());
		redirectAttrs.addFlashAttribute("password", user.getPhonenumber());
		return "redirect:/showtrain";
	}

	@RequestMapping(path = "/showtrain")
	public String showTrain(@ModelAttribute("attr") String user, @ModelAttribute("attr") String password, Model model) {
		String name = (String) model.asMap().get("username");
		log.info("Getting Username " + user);
		List<trainInfo> trainInfos = railwayservice.getAllTrainDetails();
		model.addAttribute("trainDetails", trainInfos);
		model.addAttribute("name", name);
		return "ShowTrain";
	}

	@GetMapping(path = "/bookticket/{trainid}")
	public String bookTicketusingpath(@PathVariable String trainid, Model model,
			final RedirectAttributes redirectAttrs) {
		log.info("inside ticket booking for trainid " + trainid);
		Optional<trainInfo> traininfo = railwayservice.getTrainDetailUsingID(Integer.valueOf(trainid));
		log.info("is train details available" + traininfo.isPresent());
		if (traininfo.isPresent()) {
			model.addAttribute("Trainid", traininfo.get().getTrainid());
		}
		TicketBooking ticketBooking = new TicketBooking();
		model.addAttribute("ticketbooking", ticketBooking);
		model.addAttribute("tid", trainid);
		return "ticket-reservation";
	}

	@PostMapping(path = "/ticketconfirmation")
	public String ticketConfirmation(@ModelAttribute("ticketbooking") TicketBooking ticketBooking,
			@RequestParam("tid") String trainid, BindingResult bindingResult, Model model) throws TrainInfoException {

		log.info("Ticket Details " + ticketBooking.toString()+" for Trainid "+trainid);
		Optional<trainInfo> traininfo = railwayservice.getTrainDetailUsingID(Integer.valueOf(trainid));
		trainInfo train = new trainInfo();
		if (traininfo.isPresent()) {
			train = traininfo.get();
			ticketReservationDB reservationDB = railwayservice.conformTicket(train, ticketBooking);
			System.out.println(ticketBooking.toString());
			model.addAttribute("id", reservationDB.getReserveid());
			model.addAttribute("source", train.getSourcestn());
			model.addAttribute("dest", train.getDesstn());
			model.addAttribute("arrtime", train.getArrivalString());
			model.addAttribute("deptime", train.getDepaString());
			model.addAttribute("noticket", ticketBooking.getNoofticket());
			model.addAttribute("amount", (train.getFare() * Double.valueOf(ticketBooking.getNoofticket())));
		} else {
			throw new TrainInfoException(2000, "Error occured while booking the ticket");
		}
		return "TicketConfirmation";
	}
}
