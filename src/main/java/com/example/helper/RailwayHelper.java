package com.example.helper;

import com.example.model.TicketBooking;
import com.example.model.User;
import com.example.model.passengerInfo;
import com.example.model.ticketReservationDB;
import com.example.model.trainInfo;

public class RailwayHelper {

	public static passengerInfo createPassnegerInfo(User user) {
		passengerInfo passengerInfo = new passengerInfo();
		passengerInfo.setAddress(user.getAddress());
		passengerInfo.setAge(user.getAge());
		passengerInfo.setEmailid(user.getMailid());
		passengerInfo.setSex(user.getGender());
		passengerInfo.setPhoneno(user.getPhonenumber());
		passengerInfo.setPassName(user.getName());
		return passengerInfo;
	}

	public static ticketReservationDB createTicketReservation(trainInfo traininfo, TicketBooking ticketBooking) {

		ticketReservationDB reservation=new ticketReservationDB();
		//reservation.setProofid(ticketBooking.get);
		reservation.setDate(ticketBooking.getDate());
		reservation.setProofname(ticketBooking.getProofName());
		reservation.setSource(traininfo.getSourcestn());
		reservation.setDestina(traininfo.getDesstn());
		reservation.setNoofSeatsReserved(Integer.valueOf(ticketBooking.getNoofticket()));
		reservation.setTrainid(Integer.valueOf(traininfo.getTrainid()));
		reservation.setSpare(traininfo.getFare());
		return reservation;   
	}
}
