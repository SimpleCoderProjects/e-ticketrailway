package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "passengerDB")
public class passengerInfo {

	@Id
	@Column(name = "passid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int passid;
	@Column(name = "name")
	private String PassName;
	@Column(name = "age")
	private int age;
	@Column(name = "sex")
	private String sex;
	@Column(name = "address")
	private String address;
	@Column(name = "phoneno")
	private String phoneno;
	@Column(name = "mailid")
	private String emailid;

	public passengerInfo() {
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPassId() {
		return passid;
	}

	public void setPassId(int passId) {
		this.passid = passId;
	}

	public String getPassName() {
		return PassName;
	}

	public void setPassName(String passName) {
		PassName = passName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}

	@Override
	public String toString() {
		return "passengerInfo [passid=" + passid + ", PassName=" + PassName + ", age=" + age + ", sex=" + sex
				+ ", address=" + address + ", phoneno=" + phoneno + ", emailid=" + emailid + "]";
	}

}
