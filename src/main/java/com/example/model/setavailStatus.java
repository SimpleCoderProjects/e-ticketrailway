package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seatAvailStatusDB")
public class setavailStatus {

	@Id
	@Column(name = "trainid",unique=true,nullable=false)
	private int transacid;
	@Column(name = "journeyDate")
	private String journeyString;
	@Column(name = "availseat")
	private int availableseat;
	@Column(name = "class")
	private String resclass;

	public setavailStatus() {

	}

	public int getTransacid() {
		return transacid;
	}

	public void setTransacid(int transacid) {
		this.transacid = transacid;
	}

	public String getJourneyString() {
		return journeyString;
	}

	public void setJourneyString(String journeyString) {
		this.journeyString = journeyString;
	}

	public int getAvailableseat() {
		return availableseat;
	}

	public void setAvailableseat(int availableseat) {
		this.availableseat = availableseat;
	}

	public String getResclass() {
		return resclass;
	}

	public void setResclass(String resclass) {
		this.resclass = resclass;
	}

}
