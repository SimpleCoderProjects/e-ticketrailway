package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ticketReservationDB")
public class ticketReservationDB {
	
	@Id
	@Column(name = "reserid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int reserveid;
	@Column(name = "passid")
	private int passid;
	@Column(name = "trainid")
	private int trainid;
	@Column(name = "sources")
	private String source;
	@Column(name = "dest")
	private String destina;
	@Column(name = "class")
	private String tclass;
	@Column(name = "proofid")
	private int proofid;
	@Column(name = "proofname")
	private String proofname;
	@Column(name = "date")
	private String date;
	@Column(name = "noofseatreserved")
	private int noofSeatsReserved;
	@Column(name = "fare")
	private double spare;
	@Column(name = "pnr")
	private int pnr;
	

	public ticketReservationDB() {
	}

	public int getPassid() {
		return passid;
	}

	public void setPassid(int passid) {
		this.passid = passid;
	}

	public int getTrainid() {
		return trainid;
	}

	public void setTrainid(int trainid) {
		this.trainid = trainid;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestina() {
		return destina;
	}

	public void setDestina(String destina) {
		this.destina = destina;
	}

	public String getTclass() {
		return tclass;
	}

	public void setTclass(String tclass) {
		this.tclass = tclass;
	}

	public int getProofid() {
		return proofid;
	}

	public void setProofid(int proofid) {
		this.proofid = proofid;
	}

	public String getProofname() {
		return proofname;
	}

	public void setProofname(String proofname) {
		this.proofname = proofname;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getNoofSeatsReserved() {
		return noofSeatsReserved;
	}

	public void setNoofSeatsReserved(int noofSeatsReserved) {
		this.noofSeatsReserved = noofSeatsReserved;
	}

	public double getSpare() {
		return spare;
	}

	public void setSpare(double spare) {
		this.spare = spare;
	}

	public int getPnr() {
		return pnr;
	}

	public void setPnr(int pnr) {
		this.pnr = pnr;
	}

	public int getReserveid() {
		return reserveid;
	}

	public void setReserveid(int reserveid) {
		this.reserveid = reserveid;
	}
}