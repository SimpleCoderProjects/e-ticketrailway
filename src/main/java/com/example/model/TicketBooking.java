package com.example.model;

import org.springframework.format.annotation.DateTimeFormat;

public class TicketBooking {

	private String trainid;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private String date;
	private String proofName;
	private String noofticket;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getProofName() {
		return proofName;
	}

	public void setProofName(String proofName) {
		this.proofName = proofName;
	}

	public String getNoofticket() {
		return noofticket;
	}

	public void setNoofticket(String noofticket) {
		this.noofticket = noofticket;
	}

	public String getTrainid() {
		return trainid;
	}

	public void setTrainid(String trainid) {
		this.trainid = trainid;
	}

	@Override
	public String toString() {
		return "TicketBooking [trainid=" + trainid + ", date=" + date + ", proofName=" + proofName + ", noofticket="
				+ noofticket + "]";
	}

}
