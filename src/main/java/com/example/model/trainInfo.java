package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "trainDB")
public class trainInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int trainid;
	@Column(name = "trainname")
	private String trainname;
	@Column(name = "sources")
	private String sourcestn;
	@Column(name = "dest")
	private String desstn;
	@Column(name = "depatime")
	private String depaString;
	@Column(name = "arrivaltime")
	private String arrivalString;
	@Column(name = "traintype")
	private String traintype;
	@Column(name = "distance")
	private int distance;
	@Column(name = "speed")
	private String speed;
	@Column(name = "fare")
	private double fare;
	@Column(name = "noofseats")
	private int noOfSeats;

	public trainInfo() {
	}

	public int getTrainid() {
		return trainid;
	}

	public void setTrainid(int trainid) {
		this.trainid = trainid;
	}

	public String getTrainname() {
		return trainname;
	}

	public void setTrainname(String trainname) {
		this.trainname = trainname;
	}

	public String getSourcestn() {
		return sourcestn;
	}

	public void setSourcestn(String sourcestn) {
		this.sourcestn = sourcestn;
	}

	public String getDesstn() {
		return desstn;
	}

	public void setDesstn(String desstn) {
		this.desstn = desstn;
	}

	public String getDepaString() {
		return depaString;
	}

	public void setDepaString(String depaString) {
		this.depaString = depaString;
	}

	public String getArrivalString() {
		return arrivalString;
	}

	public void setArrivalString(String arrivalString) {
		this.arrivalString = arrivalString;
	}

	public String getTraintype() {
		return traintype;
	}

	public void setTraintype(String traintype) {
		this.traintype = traintype;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getSpeed() {
		return speed;
	}

	public void setSpeed(String speed) {
		this.speed = speed;
	}

	public double getFare() {
		return fare;
	}

	public void setFare(double fare) {
		this.fare = fare;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
	
	@Override
	public String toString() {
		return "trainInfo [trainid=" + trainid + ", trainname=" + trainname + ", sourcestn=" + sourcestn + ", desstn="
				+ desstn + ", depaString=" + depaString + ", arrivalString=" + arrivalString + ", traintype="
				+ traintype + ", distance=" + distance + ", speed=" + speed + ", fare=" + fare + ", noOfSeats="
				+ noOfSeats + "]";
	}
}
