package com.example.exception;

public class UserNotFoundException extends Exception{
	
	private static final long serialVersionUID = -3213951466105459150L;
	private int code;
	private String message;
	
	public UserNotFoundException(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
