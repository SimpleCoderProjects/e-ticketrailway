package com.example.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ErrorHandler {

	public static final String HOMEPAGE = "http://localhost:8080/";

	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(HttpServletRequest request, Exception ex) {
		ex.printStackTrace();
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("errorcode", 10001);
		modelAndView.addObject("errormessage", ex.getMessage());
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", HOMEPAGE);
		modelAndView.setViewName("error");
		return modelAndView;
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ModelAndView handleException(HttpServletRequest request, UserNotFoundException ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("errorcode", ex.getCode());
		modelAndView.addObject("errormessage", ex.getMessage());
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", HOMEPAGE);
		modelAndView.setViewName("error");
		return modelAndView;
	}
}
