package com.example.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.exception.UserNotFoundException;
import com.example.helper.RailwayHelper;
import com.example.model.TicketBooking;
import com.example.model.User;
import com.example.model.passengerInfo;
import com.example.model.ticketReservationDB;
import com.example.model.trainInfo;
import com.example.repo.TicketReservationRepository;
import com.example.repo.passengerRepository;
import com.example.repo.trainRepository;

@Service
public class RailwayService {

	@Autowired
	passengerRepository passengerRepository;

	@Autowired
	trainRepository trainRepository;
	
	@Autowired
	TicketReservationRepository reservationRepository;

	public boolean isUSerValid(String username, String phoneno) throws UserNotFoundException {
		passengerInfo passengerInfo = passengerRepository.findUserByPhoneno(phoneno);

		if (passengerInfo == null) {
			throw new UserNotFoundException(1000, "No Such User Exists Please Provide Valid Username and Password");
		} else if (!passengerInfo.getPassName().equalsIgnoreCase(username)) {
			System.out.println(passengerInfo.toString());
			throw new UserNotFoundException(1001, "The given Username and password does not match with any record");
		}
		return true;
	}

	public boolean saveUserDetails(User user) throws UserNotFoundException {
		passengerInfo passenger = null;
		passenger=passengerRepository.findUserByPhoneno(user.getPhonenumber());
		if (null != passenger) {
			System.out.println("inside Save " + passenger);
			throw new UserNotFoundException(1002, "Already a User exists with given details");
		}
		System.out.println("inside Saving the new User");
		passengerInfo passengerinfo = RailwayHelper.createPassnegerInfo(user);
		passengerRepository.save(passengerinfo);
		return true;
	}

	public List<trainInfo> getAllTrainDetails() {
		return trainRepository.findAll();
	}

	public Optional<trainInfo> getTrainDetailUsingID(int id) {
		return trainRepository.findById(id);
	}
	
	public ticketReservationDB conformTicket(trainInfo traininfo,TicketBooking ticketBooking)
	{
		ticketReservationDB reservation= RailwayHelper.createTicketReservation(traininfo, ticketBooking);
		reservation =reservationRepository.save(reservation);
		return reservation;
	}
}
