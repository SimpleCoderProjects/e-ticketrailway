package com.example.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.ticketReservationDB;

@Repository
public interface TicketReservationRepository extends JpaRepository<ticketReservationDB, Integer> {
}
