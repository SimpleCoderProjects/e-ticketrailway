package com.example.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.trainInfo;

@Repository
public interface trainRepository extends JpaRepository<trainInfo, Integer> {

}
