package com.example.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.model.passengerInfo;

@Repository
public interface passengerRepository extends JpaRepository<passengerInfo, Integer>{

	@Query("SELECT p FROM passengerInfo p WHERE p.phoneno = :phoneno") 
    public passengerInfo findUserByPhoneno(@Param("phoneno") String phoneno);
	
}
